# TP Spark 

Le but de ce TP est de s'initier à l'utilisation de Spark en Scala

## Jeu de données

Nous allons disposer d'un jeu de données sur les matchs de football des principaux championnats nationaux en Europe sur les dernières années.

Il y'aura trois fichiers à utiliser :

- **League.csv** : qui contient les informations sur les championnats nationaux en Europe  
- **Team.csv** : qui contient l'ensemble des équipes qui participent aux championnats
- **Match.Csv** : qui contient l'ensemble des matchs joués dans les championnats nationaux en Europe 

Pour plus de détails sur chaque fichier, n'hésitez pas à regarder l'entête de chaque fichier

## But du TP

Il vous sera demandé d'implémenter l'ensemble des fonctions nécessaires pour répondre à cette question : 

**Quel est le TOP 10 des équipes ayant cumulé le plus gros nombre de points sur une saison ?** 

**Indice** : Le Barça apparait 4 fois dans ce top 10 et la Juventus est l'équipe qui occupe la première place.

## Déroulement du TP

- Une classe test a été mise en place pour vérifier chacune de vos implémentations, merci de lire ce que fait la classe 
test pour mieux comprendre pour chaque question, ce qui vous sera demander d'implémenter 

**Question 1** : `linesToMapOfLeagues`
- Transformez une **RDD[String]** (qui sera obtenue à partir du fichier **league.csv**) en une **paire RDD** (en Scala ça sera une **RDD[Tuple2]**)
- Le tuple sera composé de l'id de la ligue et le nom de la ligue 
- Exemple : `"1;1;Belgium Jupiler League" -> ("1", "Belgium Jupiter League")`

**Question 2** : `linesToMapOfTeams`
- Idem que la question 1 mais pour le fichier des équipes

**Question 3** :`linesToMapOfMatch`
- Transformez une **RDD[String]** (qui sera obtenue à partir du fichier **match.csv**) en une **RDD[Match]
- La case class est mise à dispo 
- Pensez à récuperer les informations importantes pour un match 
  - l'id de la ligue
  - la saison 
  - l'id de l'équipe qui joue à domicile 
  - l'id de l'équipe qui joue à l'extérieur 
  - le nombre de buts marqués par l'équipe qui joue à domicile 
  - le nombre de buts marqués par l'équipe qui joue à l'extérieur   

**Question 4** : `matchToMatchTeam`

- Eclatez une **RDD[Match]** en **RDD[MatchTeam]** 
- La case class `MatchTeam` est mise à dispo
- L'éclatement consiste à sortir deux matchs à partir d'un match, un par équipe en calculant le nombre de points de chaque équipe
- Exemple : `Match("1", "2008/2009", "9987", "9993", "1", "1")` devient 
  - `MatchTeam("1", "2008/2009", "9987", 1)` 
  - `MatchTeam("1", "2008/2009", "9993", 1)`

- Le calcul de points se fait comme cela : 
  - 0 pour une défaite 
  - 1 pour un match nul 
  - 3 pour une victoire   

**Question 5** : `teamToPair`

- Transformez une **RDD[MatchTeam]** en une **paire RDD** (en Scala ça sera une **RDD[Tuple2]**)
- Le tuple sera composé de **MatchTeam** comme clé et le **nombre de points** comme valeur
- Exemples : 
  - `MatchTeam("1", "2008/2009", "9987", 1)`  devient `(MatchTeam("1", "2008/2009", "9987", 0), 1)`
  - `MatchTeam("1", "2008/2009", "9987", 3)`  devient `(MatchTeam("1", "2008/2009", "9987", 0), 3)`  
- Notez que la clé MatchTeam a comme nombre de points égale à 0 (important car c'est notre clé)

**Question 6** : `reduce`
- Réduisez la paire `(MatchTeam, Nombre de points)` pour calculer le cumul de points pour obtenir au final des paires `(MatchTeam,Nombre de points cumulés sur une saison)` 

**Question 7** : `joinWithLeagueAndTeam`
- La paire obtenue précédemment contient les informations suivantes : `(MatchTeam("1", "2008/2009", "9985"), 6)`
- On souhaite remplacer les id des ligues et les id des équipes par les vrais noms de ces dernières pour obtenir ceci `(MatchTeam("Belgium Jupiler League", "2008/2009", "Standard de Liège"), 6)`
- Pensez à transformer les RDD de league et team en des map avec la combinaison des deux fonctions `collect()` & `toMap()`

**Question 8** : Répondre à la question principale 
- Il vous sera mis à disposition les trois fichiers
- Ecrivez le programme principal qui nous retourne le TOP 10 des équipes ayant cumulé le plus gros nombre de points sur une saison



