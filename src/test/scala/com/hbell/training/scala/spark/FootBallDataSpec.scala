package com.hbell.training.scala.spark

import com.hbell.training.scala.spark.model.{Match, MatchTeam}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfter, FlatSpec, GivenWhenThen, Matchers}


class FootBallDataSpec extends FlatSpec with BeforeAndAfter with GivenWhenThen with Matchers {

  private val master = "local[2]"
  private val appName = "example-spark"

  private var sc: SparkContext = _

  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    sc = new SparkContext(conf)
  }

  after {
    if (sc != null) {
      sc.stop()
    }
  }

  val leagues = Array(
    "id;country_id;name",
    "1;1;Belgium Jupiler League",
    "1729;1729;England Premier League"
  )

  val teams = Array(
    "id;team_api_id;team_fifa_api_id;team_long_name;team_short_name",
    "1;9987;673;KRC Genk;GEN",
    "2;9993;675;Beerschot AC;BAC",
    "3;10000;15005;SV Zulte-Waregem;ZUL",
    "4;9994;2007;Sporting Lokeren;LOK",
    "5;9984;1750;KSV Cercle Brugge;CEB",
    "6;8635;229;RSC Anderlecht;AND",
    "7;9991;674;KAA Gent;GEN",
    "8;9998;1747;RAEC Mons;MON",
    "9;7947;;FCV Dender EH;DEN",
    "10;9985;232;Standard de Liège;STL",
    "11;8203;110724;KV Mechelen;MEC",
    "12;8342;231;Club Brugge KV;CLB",
    "13;9999;546;KSV Roeselare;ROS",
    "14;8571;100081;KV Kortrijk;KOR",
    "15;4049;;Tubize;TUB",
    "16;9996;111560;Royal Excel Mouscron;MOU",
    "17;10001;681;KVC Westerlo;WES",
    "18;9986;670;Sporting Charleroi;CHA",
    "614;9997;680;Sint-Truidense VV;STT",
    "1034;9989;239;Lierse SK;LIE",
    "1042;6351;2013;KAS Eupen;EUP",
    "1513;1773;100087;Oud-Heverlee Leuven;O-H",
    "2004;8475;110913;Waasland-Beveren;WAA",
    "2476;8573;682;KV Oostende;OOS",
    "2510;274581;111560;Royal Excel Mouscron;MOP",
    "3457;10260;11;Manchester United;MUN",
    "3458;10261;13;Newcastle United;NEW",
    "3459;9825;1;Arsenal;ARS",
    "3460;8659;109;West Bromwich Albion;WBA",
    "3461;8472;106;Sunderland;SUN",
    "3462;8650;9;Liverpool;LIV",
    "3463;8654;19;West Ham United;WHU",
    "3464;8528;1917;Wigan Athletic;WIG",
    "3465;10252;2;Aston Villa;AVL",
    "3466;8456;10;Manchester City;MCI",
    "3467;8668;7;Everton;EVE",
    "3468;8655;3;Blackburn Rovers;BLB"


  )

  val matches = Array(
    "id;country_id;league_id;season;stage;date;match_api_id;home_team_api_id;away_team_api_id;home_team_goal;away_team_goal;home_player_X1;home_player_X2;home_player_X3;home_player_X4;home_player_X5;home_player_X6;home_player_X7;home_player_X8;home_player_X9;home_player_X10;home_player_X11;away_player_X1;away_player_X2;away_player_X3;away_player_X4;away_player_X5;away_player_X6;away_player_X7;away_player_X8;away_player_X9;away_player_X10;away_player_X11;home_player_Y1;home_player_Y2;home_player_Y3;home_player_Y4;home_player_Y5;home_player_Y6;home_player_Y7;home_player_Y8;home_player_Y9;home_player_Y10;home_player_Y11;away_player_Y1;away_player_Y2;away_player_Y3;away_player_Y4;away_player_Y5;away_player_Y6;away_player_Y7;away_player_Y8;away_player_Y9;away_player_Y10;away_player_Y11;home_player_1;home_player_2;home_player_3;home_player_4;home_player_5;home_player_6;home_player_7;home_player_8;home_player_9;home_player_10;home_player_11;away_player_1;away_player_2;away_player_3;away_player_4;away_player_5;away_player_6;away_player_7;away_player_8;away_player_9;away_player_10;away_player_11;B365H;B365D;B365A;BWH;BWD;BWA;IWH;IWD;IWA;LBH;LBD;LBA;PSH;PSD;PSA;WHH;WHD;WHA;SJH;SJD;SJA;VCH;VCD;VCA;GBH;GBD;GBA;BSH;BSD;BSA",
    "1;1;1;2008/2009;1;2008-08-17 00:00:00;492473;9987;9993;1;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.73;3.4;5;1.75;3.35;4.2;1.85;3.2;3.5;1.8;3.3;3.75;;;;1.7;3.3;4.33;1.9;3.3;4;1.65;3.4;4.5;1.78;3.25;4;1.73;3.4;4.2",
    "2;1;1;2008/2009;1;2008-08-16 00:00:00;492474;10000;9994;0;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.95;3.2;3.6;1.8;3.3;3.95;1.9;3.2;3.5;1.9;3.2;3.5;;;;1.83;3.3;3.6;1.95;3.3;3.8;2;3.25;3.25;1.85;3.25;3.75;1.91;3.25;3.6",
    "3;1;1;2008/2009;1;2008-08-16 00:00:00;492475;9984;8635;0;3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.38;3.3;2.75;2.4;3.3;2.55;2.6;3.1;2.3;2.5;3.2;2.5;;;;2.5;3.25;2.4;2.63;3.3;2.5;2.35;3.25;2.65;2.5;3.2;2.5;2.3;3.2;2.75",
    "4;1;1;2008/2009;1;2008-08-17 00:00:00;492476;9991;9998;5;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.44;3.75;7.5;1.4;4;6.8;1.4;3.9;6;1.44;3.6;6.5;;;;1.44;3.75;6;1.44;4;7.5;1.45;3.75;6.5;1.5;3.75;5.5;1.44;3.75;6.5"

  )

  val matches2 = Array(
    "id;country_id;league_id;season;stage;date;match_api_id;home_team_api_id;away_team_api_id;home_team_goal;away_team_goal;home_player_X1;home_player_X2;home_player_X3;home_player_X4;home_player_X5;home_player_X6;home_player_X7;home_player_X8;home_player_X9;home_player_X10;home_player_X11;away_player_X1;away_player_X2;away_player_X3;away_player_X4;away_player_X5;away_player_X6;away_player_X7;away_player_X8;away_player_X9;away_player_X10;away_player_X11;home_player_Y1;home_player_Y2;home_player_Y3;home_player_Y4;home_player_Y5;home_player_Y6;home_player_Y7;home_player_Y8;home_player_Y9;home_player_Y10;home_player_Y11;away_player_Y1;away_player_Y2;away_player_Y3;away_player_Y4;away_player_Y5;away_player_Y6;away_player_Y7;away_player_Y8;away_player_Y9;away_player_Y10;away_player_Y11;home_player_1;home_player_2;home_player_3;home_player_4;home_player_5;home_player_6;home_player_7;home_player_8;home_player_9;home_player_10;home_player_11;away_player_1;away_player_2;away_player_3;away_player_4;away_player_5;away_player_6;away_player_7;away_player_8;away_player_9;away_player_10;away_player_11;B365H;B365D;B365A;BWH;BWD;BWA;IWH;IWD;IWA;LBH;LBD;LBA;PSH;PSD;PSA;WHH;WHD;WHA;SJH;SJD;SJA;VCH;VCD;VCA;GBH;GBD;GBA;BSH;BSD;BSA",
    "1;1;1;2008/2009;1;2008-08-17 00:00:00;492473;9987;9993;1;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.73;3.4;5;1.75;3.35;4.2;1.85;3.2;3.5;1.8;3.3;3.75;;;;1.7;3.3;4.33;1.9;3.3;4;1.65;3.4;4.5;1.78;3.25;4;1.73;3.4;4.2",
    "2;1;1;2008/2009;1;2008-08-16 00:00:00;492474;10000;9994;0;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.95;3.2;3.6;1.8;3.3;3.95;1.9;3.2;3.5;1.9;3.2;3.5;;;;1.83;3.3;3.6;1.95;3.3;3.8;2;3.25;3.25;1.85;3.25;3.75;1.91;3.25;3.6",
    "3;1;1;2008/2009;1;2008-08-16 00:00:00;492475;9984;8635;0;3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.38;3.3;2.75;2.4;3.3;2.55;2.6;3.1;2.3;2.5;3.2;2.5;;;;2.5;3.25;2.4;2.63;3.3;2.5;2.35;3.25;2.65;2.5;3.2;2.5;2.3;3.2;2.75",
    "4;1;1;2008/2009;1;2008-08-17 00:00:00;492476;9991;9998;5;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.44;3.75;7.5;1.4;4;6.8;1.4;3.9;6;1.44;3.6;6.5;;;;1.44;3.75;6;1.44;4;7.5;1.45;3.75;6.5;1.5;3.75;5.5;1.44;3.75;6.5",
    "5;1;1;2008/2009;1;2008-08-16 00:00:00;492477;7947;9985;1;3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;5;3.5;1.65;5;3.5;1.6;4;3.3;1.7;4;3.4;1.72;;;;4.2;3.4;1.7;4.5;3.5;1.73;4.5;3.4;1.65;4.5;3.5;1.65;4.75;3.3;1.67",
    "6;1;1;2008/2009;1;2008-09-24 00:00:00;492478;8203;8342;1;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4.75;3.4;1.67;4.85;3.4;1.65;3.7;3.2;1.8;5;3.25;1.62;;;;4.2;3.4;1.7;5.5;3.75;1.67;4.35;3.4;1.7;4.5;3.4;1.7;;;",
    "7;1;1;2008/2009;1;2008-08-16 00:00:00;492479;9999;8571;2;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.1;3.2;3.3;2.05;3.25;3.15;1.85;3.2;3.5;1.83;3.3;3.6;;;;1.83;3.3;3.6;1.91;3.4;3.6;2.1;3.25;3;1.85;3.25;3.75;2.1;3.25;3.1",
    "8;1;1;2008/2009;1;2008-08-16 00:00:00;492480;4049;9996;1;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;3.2;3.4;2.2;2.55;3.3;2.4;2.4;3.2;2.4;2.5;3.2;2.5;;;;2.7;3.25;2.25;2.6;3.4;2.4;2.8;3.25;2.25;2.8;3.2;2.25;2.88;3.25;2.2",
    "9;1;1;2008/2009;1;2008-08-16 00:00:00;492481;10001;9986;1;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.25;3.25;2.88;2.3;3.25;2.7;2.1;3.1;3;2.25;3.2;2.75;;;;2.2;3.25;2.75;2.2;3.3;3.1;2.25;3.25;2.8;2.2;3.3;2.8;2.25;3.2;2.8",
    "10;1;1;2008/2009;10;2008-11-01 00:00:00;492564;8342;8571;4;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.3;5.25;9.5;1.25;5;10;1.3;4.2;8;1.25;4.5;10;;;;1.35;4.2;7;1.27;5;10;1.3;4.35;8.5;1.25;5;10;1.29;4.5;9",
    "11;1;1;2008/2009;10;2008-10-31 00:00:00;492565;9985;9986;1;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.3;5.25;9.5;1.25;5;10;1.3;4.2;8;1.29;4.33;9;;;;1.25;4.5;9.5;1.27;5;10;1.28;4.5;8.5;1.25;5;10;1.25;5;9",
    "12;1;1;2008/2009;10;2008-11-02 00:00:00;492566;10000;9991;0;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.6;3.2;2.5;2.6;3.2;2.4;2.4;3.2;2.4;2.5;3.2;2.5;;;;2.6;3.1;2.4;2.6;3.25;2.5;2.65;3.3;2.3;2.6;3.25;2.5;2.62;3.2;2.38",
    "13;1;1;2008/2009;10;2008-11-01 00:00:00;492567;9994;9998;0;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.91;3.4;4;1.85;3.35;3.8;1.8;3.1;3.8;1.8;3.3;3.75;;;;1.8;3.3;3.75;1.8;3.3;4.25;2;3.2;3.4;1.85;3.3;4;1.83;3.3;3.8",
    "14;1;1;2008/2009;10;2008-11-01 00:00:00;492568;7947;10001;2;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.9;3.3;2.38;2.75;3.3;2.25;2.5;3.2;2.3;2.6;3.2;2.38;;;;2.75;3.2;2.25;2.8;3.2;2.38;2.75;3.2;2.3;2.75;3.25;2.35;2.75;3.2;2.3",
    "15;1;1;2008/2009;10;2008-11-01 00:00:00;492569;8203;9999;1;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.7;3.3;4.5;1.75;3.45;4;1.7;3.2;4.2;1.67;3.3;4.5;;;;1.7;3.4;4.2;1.7;3.4;4.75;1.7;3.4;4.35;1.75;3.4;4.25;1.7;3.4;4.33",
    "16;1;1;2008/2009;10;2008-11-01 00:00:00;492570;9996;9984;0;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.35;3.3;3;2.35;3.25;2.7;2.2;3.1;2.8;2.2;3.2;2.8;;;;2.2;3.2;2.8;2.3;3.25;2.88;2.2;3.25;2.9;2.3;3.25;2.85;2.3;3.2;2.75",
    "17;1;1;2008/2009;10;2008-11-01 00:00:00;492571;4049;9987;1;3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;6;3.75;1.57;5.1;3.6;1.55;4.6;3.4;1.6;5.5;3.6;1.5;;;;5.25;3.5;1.55;5.25;3.5;1.62;5.5;3.5;1.55;5.25;3.6;1.6;6;3.6;1.5",
    "18;1;1;2008/2009;10;2008-11-02 00:00:00;492572;9993;8635;1;3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4;3.25;1.83;4.3;3.5;1.68;3.7;3.2;1.8;4;3.4;1.73;;;;4;3.3;1.75;4.25;3.3;1.8;3.6;3.25;1.9;4.1;3.4;1.8;3.8;3.3;1.83",
    "19;1;1;2008/2009;11;2008-11-08 00:00:00;492573;8635;9994;2;3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.36;4.75;8.5;1.25;5;9;1.3;4.2;8;1.36;4;7;;;;1.3;4.2;8.5;1.33;4.5;9;1.28;4.5;8.5;1.3;4.75;7.5;1.3;4.5;8.5",
    "20;1;1;2008/2009;11;2008-11-08 00:00:00;492574;9998;9996;0;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.4;3.3;2.9;2.25;3.2;2.8;2.2;3.1;2.8;2.5;3.2;2.5;;;;2.3;3.25;2.6;2.4;3.3;2.75;2.35;3.25;2.65;2.3;3.2;2.75;2.3;3.25;2.7",
    "21;1;1;2008/2009;11;2008-11-09 00:00:00;492575;9986;8342;2;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;3.2;3.3;2.25;3.25;3.2;2.1;3.1;3;2.1;2.75;3.25;2.2;;;;3;3.2;2.1;3.3;3.3;2.1;3.25;3.25;2;3.1;3.2;2.15;3.1;3.25;2.1",
    "22;1;1;2008/2009;11;2008-11-07 00:00:00;492576;9984;10000;2;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.75;3.75;4.33;1.85;3.35;3.65;1.75;3.2;4;1.73;3.4;4;;;;1.8;3.4;3.6;1.85;3.5;4.25;1.75;3.25;4.35;1.8;3.3;4.2;1.7;3.3;4.5",
    "23;1;1;2008/2009;11;2008-11-08 00:00:00;492577;9991;7947;1;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.4;4.33;8;1.38;4;7.2;1.4;3.9;6;1.33;4;8;;;;1.4;4;7;1.4;4.33;7;1.35;4;7.5;1.4;4.2;7;1.36;4.2;7",
    "24;1;1;2008/2009;11;2008-11-08 00:00:00;492578;9999;4049;1;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.83;3.5;4.2;1.8;3.35;3.9;1.85;3.2;3.5;1.73;3.4;4;;;;1.9;3.4;3.25;1.83;3.4;4;1.8;3.25;4;1.85;3.3;4;1.8;3.3;4",
    "25;1;1;2008/2009;11;2008-11-08 00:00:00;492579;8571;8203;0;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2;3.5;3.6;1.95;3.25;3.4;2;3.2;3.1;1.91;3.25;3.4;;;;2.05;3.25;3;2.05;3.25;3.5;2;3.25;3.25;1.95;3.2;3.5;2;3.25;3.3",
    "26;1;1;2008/2009;11;2008-11-08 00:00:00;492580;10001;9987;1;0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.7;3.25;2.6;2.5;3.3;2.45;2.45;3.1;2.45;2.5;3.2;2.5;;;;2.5;3.25;2.4;2.6;3.2;2.6;2.4;3.25;2.6;2.5;3.25;2.5;2.5;3.2;2.5",
    "27;1;1;2008/2009;11;2008-11-09 00:00:00;492581;9993;9985;1;3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;3.6;3.3;2.05;3.4;3.25;1.95;3.9;3.3;1.75;3.75;3.3;1.8;;;;3.25;3.25;1.95;3.6;3.3;1.95;3.75;3.25;1.85;3.6;3.25;1.95;3.5;3.3;1.91",
    "28;1;1;2008/2009;12;2008-11-16 00:00:00;492582;8342;8635;1;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.3;3.25;2.9;2.2;3.15;3;2;3.1;3.2;2.2;3.25;2.75;;;;2.15;3.1;3;2.15;3.2;3.3;2.25;3.1;2.8;2.1;3.2;3.2;2.2;3.2;2.9",
    "29;1;1;2008/2009;12;2008-11-15 00:00:00;492583;9987;9999;1;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1.25;5.25;10;1.23;5;10;1.3;4.2;8;1.25;4.5;10;;;;1.25;4.8;9;1.27;5.5;10;1.25;5;9;1.27;5;8.5;1.25;5;9",
    "30;1;1;2008/2009;12;2008-11-15 00:00:00;492584;10000;9993;2;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2;3.25;3.5;2.1;3.2;3.2;2;3.1;3.2;2;3.2;3.2;;;;1.9;3.25;3.5;2.1;3.3;3.4;2.1;3.3;3.1;2;3.2;3.5;2;3.25;3.3",

  )


  "1. Fichier League.csv" should "transforme une ligne en CV en tuple (league_id, league_name)" in {

    val l = FootballData.linesToMapOfLeagues(sc.parallelize(leagues)).collect()
    l should equal(
      Array(
        ("1", "Belgium Jupiler League"),
        ("1729", "England Premier League")
      )
    )
  }

  "2. Fichier Team.csv" should "transforme une ligne en CV en tuple (team_id, team_name)" in {

    val t = FootballData.linesToMapOfTeams(sc.parallelize(teams)).collect()
    Array(t(3), t(4)) should equal(
      Array(
        ("9994", "Sporting Lokeren"),
        ("9984", "KSV Cercle Brugge")
      )
    )
  }

  "3. Fichier Match.csv" should "transforme une ligne en CV en RDD[Match]" in {

    val m = FootballData.linesToMapOfMatch(sc.parallelize(matches)).collect()
    m should equal(
      Array(
        Match("1", "2008/2009", "9987", "9993", "1", "1"),
        Match("1", "2008/2009", "10000", "9994", "0", "0"),
        Match("1", "2008/2009", "9984", "8635", "0", "3"),
        Match("1", "2008/2009", "9991", "9998", "5", "0")
      )
    )
  }

  "4. Match vers Equipe " should "Extraire les deux équipes d'un match et calculer le nombre de point de chaque équipe" in {

    val m = FootballData.linesToMapOfMatch(sc.parallelize(matches))
    val mt = FootballData.matchToMatchTeam(m).collect

    mt should equal(Array(
      MatchTeam("1", "2008/2009", "9987", 1),
      MatchTeam("1", "2008/2009", "9993", 1),

      MatchTeam("1", "2008/2009", "10000", 1),
      MatchTeam("1", "2008/2009", "9994", 1),

      MatchTeam("1", "2008/2009", "9984"),
      MatchTeam("1", "2008/2009", "8635", 3),

      MatchTeam("1", "2008/2009", "9991", 3),
      MatchTeam("1", "2008/2009", "9998")
    ))

  }

  "5. Paires de Match" should "tranforme une RDD[MatchTeam] en une RDD de tuple [(MatchTeam,Int)] avec Le score comme valeur" in {

    val m = FootballData.linesToMapOfMatch(sc.parallelize(matches))
    val mt = FootballData.matchToMatchTeam(m)
    val pairMatchTeam = FootballData.teamToPair(mt).collect

    pairMatchTeam should equal(Array(
      (MatchTeam("1", "2008/2009", "9987"), 1),
      (MatchTeam("1", "2008/2009", "9993"), 1),

      (MatchTeam("1", "2008/2009", "10000"), 1),
      (MatchTeam("1", "2008/2009", "9994"), 1),

      (MatchTeam("1", "2008/2009", "9984"), 0),
      (MatchTeam("1", "2008/2009", "8635"), 3),

      (MatchTeam("1", "2008/2009", "9991"), 3),
      (MatchTeam("1", "2008/2009", "9998"), 0)
    ))


  }


  "6. Reduction des Paires" should "reduire les paire pour avoir le cumul de points" in {

    val m = FootballData.linesToMapOfMatch(sc.parallelize(matches2))
    val mt = FootballData.matchToMatchTeam(m)
    val pairMatchTeam = FootballData.teamToPair(mt)
    val reduce = FootballData.reduce(pairMatchTeam).collect

    Array(reduce(14), reduce(3)) should equal(
      Array(
        (MatchTeam("1", "2008/2009", "9985"), 6)
        , (MatchTeam("1", "2008/2009", "10001"), 7)
      )
    )
  }


  "7. Joindre " should "pour remplacer l'id de la ligue et l'id de l'équipe par les noms de la ligue et de l'équipe " in {

    val m = FootballData.linesToMapOfMatch(sc.parallelize(matches2))
    val mt = FootballData.matchToMatchTeam(m)
    val pairMatchTeam = FootballData.teamToPair(mt)

    val reduce = FootballData.reduce(pairMatchTeam)

    val l = FootballData.linesToMapOfLeagues(sc.parallelize(leagues))
    val t = FootballData.linesToMapOfTeams(sc.parallelize(teams))

    val withNames = FootballData.joinWithLeagueAndTeam(reduce, l, t).collect

    Array(withNames(14), withNames(3)) should equal(
      Array(
        (MatchTeam("Belgium Jupiler League", "2008/2009", "Standard de Liège"), 6)
        , (MatchTeam("Belgium Jupiler League", "2008/2009", "KVC Westerlo"), 7)
      )
    )
  }


}