package com.hbell.training.scala.spark

import com.hbell.training.scala.spark.model.{Match, MatchTeam}
import org.apache.spark.rdd.RDD

object FootballData {


  def linesToMapOfLeagues(lines: RDD[String]): RDD[Tuple2[String, String]] = ???


  def linesToMapOfTeams(lines: RDD[String]): RDD[Tuple2[String, String]] = ???


  def linesToMapOfMatch(lines: RDD[String]): RDD[Match] = ???


  def matchToMatchTeam(rddOfMatch: RDD[Match]): RDD[MatchTeam] = ???


  def teamToPair(matchTeams: RDD[MatchTeam]): RDD[(MatchTeam, Int)] = ???


  def reduce(pair: RDD[(MatchTeam, Int)]): RDD[(MatchTeam, Int)] = ???


  def joinWithLeagueAndTeam(reduce: RDD[(MatchTeam, Int)], l: RDD[(String, String)], t: RDD[(String, String)]): RDD[(MatchTeam, Int)] = ???


}

