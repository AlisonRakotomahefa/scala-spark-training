import org.apache.spark.{SparkConf, SparkContext}

object main extends App {


  val appName = "SparkTraining"
  val master = "local[4]"
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)

}
